package mayfly.core.log;

/**
 * 日志类型
 *
 * @author meilin.huang
 * @date 2020-03-06 9:54 上午
 */
public enum LogTypeEnum {
    /**
     * 正常日志
     */
    NORMAN,
    /**
     * 异常日志
     */
    EXCEPTION;
}
