package mayfly.sys.module.sys.mapper;

import mayfly.core.base.mapper.BaseMapper;
import mayfly.sys.module.sys.entity.RoleResourceDO;

/**
 * @author meilin.huang
 * @version 1.0
 * @date 2018-12-07 4:15 PM
 */
public interface RoleResourceMapper extends BaseMapper<Integer, RoleResourceDO> {
}
